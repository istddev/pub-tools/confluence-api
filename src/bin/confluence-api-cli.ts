#!/usr/bin/env node
"use strict"

/**
 * Node Package Modules
 */
import { argv } from 'process';

/**
 * CLI Library Modules
 */
import { getPageMd } from '../lib';

//
// START CLI Script
//

if (argv[2] || !argv[4]) {
    console.log(getPageMd(argv[2], argv[3]));
} else {
    console.error('Excact 2 parms required <nameSpace> <pageTitle>');
}